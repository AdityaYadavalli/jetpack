Jetpack Joyride
=========================
This is a replica of the famous arcade game - Jetpack Joyride using OpenGL.

This code is adapted from [here](https://github.com/meghprkh/graphics-boilerplate).

## To run

1. `mkdir build`
2. `cd build`
3. `cmake ../`
4. `make all -j 4`

**Note:** In case of Experimental Error for GLM.
add "#define GLM_ENABLE_EXPERIMENTAL" in the files main.h nonedit.cpp other_handlers.cpp and input.cpp, before you include glm.

**Cheers! Have Fun** 
